import json


class ClientConfig:
    file = None
    config = {}

    def __init__(self, file=None):
        self.file = file
        if self.file is None:
            raise Exception("'file' parameter cannot by 'None'")

        with open(self.file) as fin:
            try:
                self.config = json.loads(fin.read())
            except Exception:
                raise Exception("Could not parse file: " + self.file, IOError)

    def get_region(self):
        return self.config["region"]

    def get_environment(self):
        return self.config["environment"]

    def get_site_id(self):
        return self.config["site_id"]