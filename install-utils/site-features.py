import csv
import json

from install_api.api import InstallApi
from config.client_config import ClientConfig

config = ClientConfig(file='config.json')
installApi = InstallApi(config.get_environment, config.get_region)

systems = None
all_features = []
header = ['system', 'site']
sites = []

response = installApi.get_systems()

if response.status_code == 200:
    systems = json.loads(response.text)

response = installApi.get_features()

if response.status_code == 200:
    features_response = json.loads(response.text)
    for f in features_response['nonsensitive']['allFeatures']:
        all_features.append(f['feature_code'])

if systems is not None and len(all_features) > 0:
    sites_response = systems
    for system in sites_response['nonsensitive']['system_site']:
        try:
            for site in system['sites']:
                s = {
                    "system": system["system_display_name"],
                    "site": site["site_display_name"]
                }
                for f in all_features: s[f] = 'F'
                for sf in site['site_feature_code_list']: s[sf] = 'T'
                sites.append(s)
        except KeyError:
            continue

    header = header + all_features

with open('site_features.csv', 'w') as csvfile:
    fieldnames = header
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    for site in sites:
        writer.writerow(site)
