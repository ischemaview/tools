# RapidAI Install API python utils

## Setup

### Requirements

* Python 3.7+ 

### Virtual Env
The scripts included in this package should be run using Python virtual env.  To create a virtual environment for this package, please run:

```commandline
python3 -m venv .
```

### Install requirements
```commandline
pip install -r requirements.txt
```

### Configure AWS cli
The scripts rely on the AWS cli configuration to properly target the environment and authorize the requests.

### Scripts configuration
This package includes a configuration file: **config.json**.  The configuration in the file will apply to all scripts in this package.  The following parameters need to be set in the file:
* **region** - Refers to the Amazon environment targeted.  E.g. *us-west-2*
* **environment** - Refers to the target RapidAI AWS environment. E.g. *sandpit*, *qa*, *preview*, *prod*

## Scripts

### Site Features

#### script file

site-features.py

#### description
This util script returns all sites associated with the environment and all the features enabled for the site

#### run
```commandline
python site-features.py
```

#### output
The script outputs **site_features.csv** file that lists all the features and sites with a "T" if the site has the feature enabled and an "F" if the feature is not enabled.

### User Privileges

#### script file

user-privileges.py

#### description
This util script returns all user teams and privileges for all users at a user-supplied site

#### run
```commandline
python site-features.py 
```



**Note** please update config.json to add a default site_id

#### output
The script outputs **site_features.csv** file that lists all the features and sites with a "T" if the site has the feature enabled and an "F" if the feature is not enabled.
