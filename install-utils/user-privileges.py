import json
import os
import sys

import argparse
from install_api.api import InstallApi
from config.client_config import ClientConfig

config = ClientConfig(file="config.json")
installApi = InstallApi(config.get_environment(), config.get_region())
users = {}

parser = argparse.ArgumentParser(description='User Privilege util arguments')
parser.add_argument('--site_code', help='Optional site_code')

args = parser.parse_args()
site_code = args.site_code
site_id = None

if site_code is not None:
    site = installApi.get_site_by_site_code(site_code)
    site_id = site["site_id"]
else:
    site_id = config.get_site_id()

if site_id is None or site_id == "":
    print("No site id set, exiting...")
    sys.exit(1)

response = installApi.get_all_users()
users_response = json.loads(response.text)

for user in users_response["nonsensitive"]["users"]:
    users[user["cognito_username"]] = user

response = installApi.get_teams_at_site(site_id)
teams_response = json.loads(response.text)

rows = []
user_output = {}
for team in teams_response["nonsensitive"]["teams"]:
    for u in team["users"]:
        username = u["username"]
        if username in user_output.keys():
            user_output[username]["permissions"].extend(u["access_privileges"])
            user_output[username]["team"].extend([team["team_type"]])
        else:
            user_output[username] = {
                "username": username,
                "name": u["full_name"],
                "role": u["user_role"],
                "phone": users[username]["phone_number"],
                "email": users[username]["email"],
                "permissions": u["access_privileges"],
                "team": [team["team_type"]]
            }

with open("team_priviledges.csv", "w") as csvfile:
    for u in user_output.keys():
        line = u + "," + \
               user_output[u]["name"] + "," +\
               user_output[u]["role"] + "," +\
               user_output[u]["phone"] + "," +\
               user_output[u]["email"] + "," +\
               ",".join(user_output[u]["permissions"]) + ","  + ",".join(user_output[u]["team"]) +\
               os.linesep
        csvfile.write(line)