import json

import requests
from aws_requests_auth.boto_utils import BotoAWSRequestsAuth


class InstallApi:
    env = ""
    region = ""
    host = "api." + region + "." + env + ".rapid-sys.com"
    base_url = ""
    system_url = "https://" + host + "/install/systems"
    feature_url = "https://" + host + "/install/features"
    headers = {
        'Content-Type': 'application/json',
    }

    def __init__(self, env, region):
        self.env = env
        self.region = region
        self.host = "api." + region + "." + env + ".rapid-sys.com"
        self.base_url = "https://" + self.host + "/install"
        self.system_url = self.base_url + "/systems"
        self.feature_url = self.base_url + "/features"
        self.user_site_url = self.base_url + "/user-sites"
        self.teams_url = self.base_url + "/teams"
        self.users_url = self.base_url + "/users"

    def get_host(self):
        return self.host

    def get_systems(self):
        return self.__send_request("GET", self.system_url)

    def get_features(self):
        return self.__send_request("GET", self.feature_url)

    def get_user_site_by_site(self, site_id):
        params = {"site_id": site_id, "group_by": "site"}
        url = self.user_site_url + self.__construct_query_param_string(params)
        print(url)
        return self.__send_request("GET", url)

    def get_teams_at_site(self, site_id):
        params = {"site_id": site_id}
        url = self.teams_url + self.__construct_query_param_string(params)
        return self.__send_request("GET", url)

    def get_all_users(self):
        return self.__send_request("GET", self.users_url)

    def get_site_by_site_code(self, site_code):
        systems_response = json.loads(self.get_systems().text)
        for system in systems_response['nonsensitive']['system_site']:
            try:
                for site in system['sites']:
                    if site['site_code'] == site_code:
                        return site
            except KeyError:
                continue

    def __send_request(self, method, url):
        auth = BotoAWSRequestsAuth(aws_host=self.host, aws_region=self.region, aws_service='execute-api')
        response = requests.request(method, url, auth=auth)
        return response

    def __construct_query_param_string(self, params):
        query_string = "?"
        param_pairs = map(lambda x: x + "=" + params[x], params.keys())
        return query_string + "&".join(param_pairs)
